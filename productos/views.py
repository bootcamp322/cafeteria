from django.shortcuts import render
from rest_framework import viewsets
from .models import Producto
from .serializers import ProductoSerializer
from cefeteriaclase.permissions import IsRecepcionista

class ProductosViewSet(viewsets.ModelViewSet):
    serializer_class = ProductoSerializer
    queryset =  Producto.objects.all()
    permission_classes = [IsRecepcionista]
